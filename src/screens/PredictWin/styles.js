import { StyleSheet } from 'react-native'
import Constants from '../../contants'

const styles = StyleSheet.create({
    container: {
        height: Constants.BaseStyle.DEVICE_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },
})

export default styles
