import React, { Component } from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { login, showLoader } from '../../../redux/actions/authActions'
import styles from '../styles'

export class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
        }
    }

    onLogin = () => {
        const { email, password } = this.state
        this.props.login({ email, password })
    }

    render() {
        return (
            <View style={{}}>
                <Text> Welcome Back </Text>
                <TextInput
                    placeholder="Email"
                    onChangeText={(email) => this.setState({ email })}
                    autoCapitalize={'none'}
                />
                <TextInput
                    placeholder="Password"
                    autoCapitalize={'none'}
                    onChangeText={(password) => this.setState({ password })}
                />
                <TouchableOpacity onPress={this.onLogin}>
                    <Text>Login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = ({ auth: { isLoading } }) => ({
    isLoading,
})

const mapDispatchToProps = {
    login: login,
    showLoader: showLoader,
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
