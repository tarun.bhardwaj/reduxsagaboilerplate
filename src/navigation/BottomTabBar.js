import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { Routes, RoutesName } from './routes.config'
import Layout from './Layout'

const Tab = createMaterialBottomTabNavigator()

class BottomTabBar extends Component {
    render() {
        return (
            <Tab.Navigator>
                <Tab.Screen
                    name={RoutesName.ScoreSchedules}
                    options={Routes.ScoreSchedules.option}
                >
                    {(props) => (
                        <Layout router={Routes.ScoreSchedules} {...props} />
                    )}
                </Tab.Screen>
                <Tab.Screen
                    name={RoutesName.PredictWin}
                    options={Routes.PredictWin.option}
                >
                    {(props) => (
                        <Layout router={Routes.PredictWin} {...props} />
                    )}
                </Tab.Screen>
            </Tab.Navigator>
        )
    }
}

export default BottomTabBar
