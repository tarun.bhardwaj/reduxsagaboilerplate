import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import BottomTabBar from './BottomTabBar'

const Stack = createStackNavigator()

export class AppStack extends Component {
    render() {
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Home" component={BottomTabBar} />
            </Stack.Navigator>
        )
    }
}

export default AppStack
