import { Login } from '../screens/Auth'
import { PredictWin } from '../screens/PredictWin'
import { ScoreSchedules } from '../screens/ScoreSchedules'

export const RoutesName = {
    LOGIN: 'login',
    ScoreSchedules: 'scoreSchedules',
    PredictWin: 'predictWin',
}

export const Routes = {
    LOGIN: {
        name: RoutesName.LOGIN,
        component: Login,
        safeArea: true,
        options: {
            headerShown: false,
        },
    },
    ScoreSchedules: {
        name: RoutesName.ScoreSchedules,
        component: ScoreSchedules,
        safeArea: true,
        option: {
            headerShown: false,
        },
    },
    PredictWin: {
        name: RoutesName.PredictWin,
        component: PredictWin,
        safeArea: true,
        option: {
            headerShown: false,
        },
    },
}
