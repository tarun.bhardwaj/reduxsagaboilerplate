import React, { Component } from 'react'
import { SafeAreaView, Text } from 'react-native'

class Layout extends Component {
    render() {
        const { router } = this.props

        return router?.safeArea ? (
            <SafeAreaView>
                <router.component />
            </SafeAreaView>
        ) : (
            <router.component />
        )
    }
}

export default Layout
