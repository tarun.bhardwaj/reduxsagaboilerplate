import { createStackNavigator } from '@react-navigation/stack'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Layout from './Layout'
import { Routes, RoutesName } from './routes.config'

const Stack = createStackNavigator()

class AuthStack extends Component {
    render() {
        return (
            <Stack.Navigator>
                <Stack.Screen
                    name={RoutesName.LOGIN}
                    options={Routes.LOGIN.options}
                >
                    {(props) => <Layout router={Routes.LOGIN} {...props} />}
                </Stack.Screen>
            </Stack.Navigator>
        )
    }
}

export default AuthStack
