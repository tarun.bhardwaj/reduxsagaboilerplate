import AsyncStorage from '@react-native-community/async-storage'
import { persistCombineReducers } from 'redux-persist'
import auth from './authReducer'
import network from './networkReducer'

const config = {
    // blacklist: ['auth', 'user', 'profile'],
    key: 'root',
    storage: AsyncStorage,
}

const reducers = persistCombineReducers(config, {
    auth,
    network,
})

export default reducers
