import {
    HIDE_LOADER,
    LOGIN_REQUESTED,
    LOGIN_SUCCESS,
    REMOVE_AUTH_TOKEN,
    SET_LOGIN_DETAILS,
    SHOW_LOADER,
} from '../actionsTypes/authActionsTypes'

const initalState = {
    token: '',
    user_id: null,
    role: null,
    email: '',
    device_token: null,
    intro: false,
    user: null,
    isLoading: false,
}

export default function auth(state = initalState, action) {
    switch (action.type) {
        case LOGIN_REQUESTED:
            return {
                ...state,
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                user: action.payload,
                token: action.payload.token,
            }
        case SET_LOGIN_DETAILS:
            return {
                ...state,
                ...payload,
            }
        case REMOVE_AUTH_TOKEN:
            return {
                ...state,
                token: '',
            }
        case SHOW_LOADER:
            return { ...state, isLoading: true }
        case HIDE_LOADER:
            return { ...state, isLoading: false }
        default:
            return state
    }
}
