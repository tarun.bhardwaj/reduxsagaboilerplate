import { all, call, put, takeLatest, takeLeading } from 'redux-saga/effects'

import httpClient from './http-client'
import { Alert } from 'react-native'
import {
    setAuthToken,
    setUserCred,
    setUserId,
} from '../../utils/helpers/AuthHelper'
import {
    loginFailure,
    loginRequest,
    loginSuccess,
} from '../actions/authActions'
import { LOGIN } from '../actionsTypes/authActionsTypes'

export function* login(data) {
    console.log(data)
    yield put(loginRequest())

    try {
        const { error, result } = yield call(httpClient, {
            data: data.payload,
            method: 'post',
            url: '/login',
        })

        console.log('RESULT: ', error, result)

        if (error) {
            yield put(loginFailure(error))
        }

        if (result.code === 200) {
            setAuthToken(result.data.token)
            setUserId(result.data.user_id.toString())
            // setUserCred(data.payload)
            yield put(loginSuccess(result.data))
        } else {
            yield put(loginFailure(error))
            Alert.alert(result.message)
        }
    } catch (error) {
        Alert.alert('Login', 'Something went wrong, Please try again later')
    }
}

function* Auth() {
    yield all([takeLatest(LOGIN, login)])
}

export default Auth
