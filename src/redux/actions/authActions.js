import { createAction } from 'redux-actions'
import {
    HIDE_LOADER,
    LOGIN,
    LOGIN_FAILURE,
    LOGIN_REQUESTED,
    LOGIN_SUCCESS,
    LOGOUT,
    SET_LOGIN_DETAILS,
    SHOW_LOADER,
} from '../actionsTypes/authActionsTypes'

export const setLoginDetails = createAction(SET_LOGIN_DETAILS)

export const showLoader = createAction(SHOW_LOADER)

export const hideLoader = createAction(HIDE_LOADER)

export const login = createAction(LOGIN)

export const logout = createAction(LOGOUT)

export const loginRequest = createAction(LOGIN_REQUESTED)

export const loginSuccess = createAction(LOGIN_SUCCESS)

export const loginFailure = createAction(LOGIN_FAILURE)
