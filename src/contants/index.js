import BaseStyle from './baseStyles'
import Colors from './colors'
import API from './apis'

const Constants = {
    BaseStyle,
    Colors,
    API,
}

export default Constants
